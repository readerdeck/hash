import sbt._
import sbt.Classpaths.publishTask
import Keys._
import com.typesafe.sbt.SbtMultiJvm
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.{ MultiJvm, extraOptions, jvmOptions, scalatestOptions, multiNodeExecuteTests, multiNodeJavaName, multiNodeHostsFileName, multiNodeTargetDirName, multiTestOptions }
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import sbtassembly.Plugin._
import AssemblyKeys._
import com.typesafe.sbt.SbtStartScript
import com.twitter.scrooge.ScroogeSBT

object HashBuild extends Build {

  lazy val root = Project("root", file("."), settings = rootSettings) aggregate(core)
  lazy val core = Project("core", file("core"), settings = coreSettings)

  def commonSettings = Defaults.defaultSettings ++ Seq(
    organization := "com.readerdeck",
    version := "1.0.0",
    scalaVersion in ThisBuild := "2.10.2",
    scalacOptions := Seq("-unchecked", "-optimize", "-deprecation", "-feature", "-language:higherKinds", "-language:implicitConversions", "-language:postfixOps", "-language:reflectiveCalls", "-Yinline-warnings", "-encoding", "utf8"),
    retrieveManaged := true,

    fork := true,
    javaOptions += "-Xmx2500M",

    resolvers ++= Seq(
      "anormcypher" at "http://repo.anormcypher.org/"
    ),

    publishMavenStyle := true
  ) ++ net.virtualvoid.sbt.graph.Plugin.graphSettings

  def rootSettings = commonSettings ++ Seq(
    publish := {}
  )

  def coreSettings = commonSettings ++ Seq(
    name := "hash-core",

    publishTo := Some(Resolver.url("readerdeck-releases", new URL("http://repo.readerdeck.com/artifactory/readerdeck-releases"))),

    libraryDependencies ++= Seq(
      "com.mchange"         % "c3p0" % "0.9.5-pre6",
      "joda-time"           % "joda-time" % "2.3",
      "org.joda"            % "joda-convert" % "1.6",
      "com.typesafe"        % "config" % "1.2.0",

      "com.typesafe.slick"  %% "slick"                      % "2.0.0",
      "org.typelevel"       %% "scalaz-contrib-210"         % "0.1.5",
      "org.typelevel"       %% "scalaz-contrib-validation"  % "0.1.5",
      "org.typelevel"       %% "scalaz-contrib-undo"        % "0.1.5",
      "org.typelevel"       %% "scalaz-lift"                % "0.1.5",
      "org.typelevel"       %% "scalaz-nscala-time"         % "0.1.5",
      "org.typelevel"       %% "scalaz-spire"               % "0.1.5",
      "org.msgpack"         %% "msgpack-scala"              % "0.6.8",
      "io.argonaut"         %% "argonaut"                   % "6.0.3"
    )
  ) ++ assemblySettings ++ extraAssemblySettings ++ SbtStartScript.startScriptForClassesSettings

  def extraAssemblySettings() = Seq(test in assembly := {}) ++ Seq(
    mergeStrategy in assembly := {
      case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
      case m if m.toLowerCase.matches("meta-inf.*\\.sf$") => MergeStrategy.discard
      case "reference.conf" => MergeStrategy.concat
      case _ => MergeStrategy.first
    }
  )
}