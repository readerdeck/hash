package com.readerdeck.hash

import scala.util.Try
import scala.slick.jdbc.ResultSetConcurrency
import com.mchange.v2.c3p0.ComboPooledDataSource
import scala.slick.driver.MySQLDriver.simple._

/**
 * Instantiate db connection pool and slick Database instance for global access
 *
 * @author Kumar Ishan (@kumarishan)
 */
object DB {
  private val cpds = new ComboPooledDataSource
  cpds.setDriverClass("com.mysql.jdbc.Driver")
  cpds.setJdbcUrl("jdbc:mysql://localhost:3306/readerdeck")
  cpds.setUser("readerdeck")
  cpds.setPassword("readerdeck")
  cpds.setMinPoolSize(4)
  cpds.setAcquireIncrement(5)
  cpds.setMaxPoolSize(20)

  private val db = Database.forDataSource(cpds)

  private def rawSession = db.createSession

  /**
   * Usage .. DB { implicit session =>
   *   ... all query execution goes here
   * }
   */
  def apply[T](f: Database => T): T = f(db)

  def execute[T](f: Session => T): T = db.withSession {
    session => f(session)
  }

  def readOnly[T](f: Session => T): Try[T] = {
    val ro = rawSession.forParameters(rsConcurrency = ResultSetConcurrency.ReadOnly)
    val res = Try { f(ro) }
    ro.close
    res
  }

  def readWrite[T](f: Session => T): Try[T] = {
    val rw = rawSession.forParameters(rsConcurrency = ResultSetConcurrency.Updatable)
    val res = Try {
      rw.withTransaction {
          f(rw)
      }
    }
    rw.close
    res
  }
}