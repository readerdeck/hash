package com.readerdeck.hash.implicits

import com.readerdeck.hash._
import tables._
import com.readerdeck.hash.utils.Builder._

trait BuilderImplicits {

  implicit val tuple2EntryBuilder =
    new Tuple2EntryBuilder[FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE](
      None, None, None, None,
      None, None, None, None, None
    )

  implicit val tuple3EntryBuilder =
    new Tuple3EntryBuilder[FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE](
      None, None, None, None, None,
      None, None, None, None, None
    )

  implicit val tuple4EntryBuilder =
    new Tuple4EntryBuilder[FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE](
      None, None, None, None, None,
      None, None, None, None, None, None
    )

  implicit def tuple2EntryBuildEnabled(builder: TupleEntryBuilder[
    Tuple2Entry, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE]) =
    new {
      def build = {
        new Tuple2Entry(
          builder.keyTypeId.get,
          builder.keyId.get,
          builder.version.get,
          builder.format.get,
          builder.compression.get,
          builder.value.get,
          if(builder.deleted.get) 'Y' else 'N',
          builder.createdDate,
          builder.updatedDate.get)
      }
    }

  implicit def tuple3EntryBuildEnabled(builder: TupleEntryBuilder[
    Tuple3Entry, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE]) =
    new {
      def build = {
        new Tuple3Entry(
          builder.keyTypeId.get,
          builder.keyId.get,
          builder.entityId.get,
          builder.version.get,
          builder.format.get,
          builder.compression.get,
          builder.value.get,
          if(builder.deleted.get) 'Y' else 'N',
          builder.createdDate,
          builder.updatedDate.get)
      }
    }

  implicit def tuple4EntryBuildEnabled(builder: TupleEntryBuilder[
    Tuple4Entry, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE]) =
    new {
      def build = {
        new Tuple4Entry(
          builder.keyTypeId.get,
          builder.keyId.get,
          builder.entityId.get,
          builder.subEntityId.get,
          builder.version.get,
          builder.format.get,
          builder.compression.get,
          builder.value.get,
          if(builder.deleted.get) 'Y' else 'N',
          builder.createdDate,
          builder.updatedDate.get)
      }
    }
}