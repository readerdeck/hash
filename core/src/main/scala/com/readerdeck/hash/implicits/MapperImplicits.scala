package com.readerdeck.hash.implicits

import com.readerdeck.hash._
import com.readerdeck.hash.auth._
import tables._
import scala.slick.driver.MySQLDriver.simple._
import org.joda.time._
import java.sql.{Timestamp, Blob}
import javax.sql.rowset.serial.SerialBlob

trait MapperImplicits {
  implicit def datatimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      t => new DateTime(t.getTime))

  // Tuple Table Mappers
  implicit def binaryMapper =
    MappedColumnType.base[Binary, Blob](b => new SerialBlob(b.value), s => Binary(s.getBytes(1, s.length.toInt)))

  implicit def keyTypeIdMapper = MappedColumnType.base[KeyTypeId, Short](_.id, KeyTypeId.apply _)
  implicit def idMapper = MappedColumnType.base[Id, Long](_.id, Id.apply _)
  implicit def versionMapper = MappedColumnType.base[Version, Short](_.v, Version.apply _)
  implicit def formatMapper = MappedColumnType.base[Format, Char](_.f, Format.apply _)
  implicit def compressionMapper = MappedColumnType.base[Compression, Char](_.c, Compression.apply _)

  // Auth Table Mappers
  implicit def providerUserIdMapper = MappedColumnType.base[ProviderUserId, String](_.value, ProviderUserId.apply _)
  implicit def providerIdMapper = MappedColumnType.base[ProviderId, String](_.value, ProviderId.apply _)
  // implicit def authMethodMapper = MappedColumnType.base[AuthMethod, String](_.value, AuthMethod.apply _)

  implicit def tokenMapper = MappedColumnType.base[Token, String](_.value, Token.apply _)
  implicit def secretMapper = MappedColumnType.base[Secret, String](_.value, Secret.apply _)

  implicit def accessTokenMapper = MappedColumnType.base[AccessToken, String](_.value, AccessToken.apply _)
  implicit def tokenTypeMapper = MappedColumnType.base[TokenType, String](_.value, TokenType.apply _)
  implicit def expiresInMapper = MappedColumnType.base[ExpiresIn, Int](_.value, ExpiresIn.apply _)
  implicit def refreshTokenMapper = MappedColumnType.base[RefreshToken, String](_.value, RefreshToken.apply _)

  implicit def hashMapper = MappedColumnType.base[Hash, Array[Byte]](_.value, Hash.apply _)
  implicit def saltMapper = MappedColumnType.base[Salt, Array[Byte]](_.value, Salt.apply _)
  implicit def iterationMapper = MappedColumnType.base[Iteration, Int](_.iter, Iteration.apply _)
}