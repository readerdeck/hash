package com.readerdeck.hash.tables

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.{ProvenShape, ForeignKeyQuery}
import org.joda.time._
import com.readerdeck.hash.Implicits._
import com.readerdeck.hash.utils.Builder._

case class Tuple2Entry(keyTypeId: KeyTypeId, keyId: Id,
                       version: Version, format: Format, compression: Compression, value: Binary,
                       isDeleted: Char, createdDate: Option[DateTime], updatedDate: DateTime)
              extends TupleEntry

class Tuple2EntryBuilder[HKT, HK, HV, HF, HC, HVL, HD, HUD](
    keyTypeId: Option[KeyTypeId],
    keyId: Option[Id],
    version: Option[Version],
    format: Option[Format],
    compression: Option[Compression],
    value: Option[Binary],
    deleted: Option[Boolean],
    createdDate: Option[DateTime],
    updatedDate: Option[DateTime]
  )
  extends TupleEntryBuilder[
    Tuple2Entry,
    HKT, HK, TRUE, TRUE, HV, HF, HC, HVL, HD, TRUE, HUD](
      keyTypeId,
      keyId,
      None,
      None,
      version,
      format,
      compression,
      value,
      None,
      None,
      updatedDate
  )

/**
 * this table should be created using this ddl
 *
 * create table if not exists `key_tuple_2` (
 *   `key_type_id`     smallint unsigned not null,
 *   `key_id`       bigint unsigned not null,
 *   `created_dt`   bigint unsigned not null,
 *   `updated_dt`   bigint unsigned not null,
 *   `version`      bigint unsigned not null,
 *   `is_deleted`   char(1) not null default 'N',
 *   `format`       char(1) not null default 'S',
 *   `compression`  char(1) not null default 'F',
 *   `value`        blob not null,
 *   PRIMARY KEY(`key_type`, `key_id`),
 *   INDEX(`updated_dt`)
 * ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC CHARACTER SET utf8;
 *
 * @author Kumar Ishan (@kumarishan)
 */
class Tuple2TableDef(tag: Tag) extends Table[Tuple2Entry](tag, "key_tuple_2") with TupleTable {

  type Entry = Tuple2Entry

  def keyTypeId     = column[KeyTypeId]("key_type_id", O.NotNull)
  def keyId         = column[Id]("key_id", O.NotNull)

  def version       = column[Version]("version", O.NotNull)
  def format        = column[Format]("format", O.NotNull)
  def compression   = column[Compression]("compression", O.NotNull)
  def value         = column[Binary]("value", O.DBType("blob"), O.NotNull)

  def isDeleted     = column[Char]("is_deleted", O.NotNull, O.Default('N'))
  def createdDate   = column[DateTime]("created_dt", O.NotNull)
  def updatedDate   = column[DateTime]("updated_dt", O.NotNull)

  def pk = primaryKey("pk_a", (keyTypeId, keyId))
  def idx = index("idx_a", (updatedDate), unique = false)

  def * =
    (keyTypeId, keyId, version, format, compression, value, isDeleted, createdDate.?, updatedDate) <>
      (Tuple2Entry.tupled, Tuple2Entry.unapply)
}