package com.readerdeck.hash.tables

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape
import org.joda.time._
import com.readerdeck.hash.Implicits._
import com.readerdeck.hash.auth.Implicits._
import com.readerdeck.hash.auth._

case class AuthEntry(uid: Id,
                     provider: Provider,
                     isDeleted: Char, createdDate: Option[DateTime], updatedDate: DateTime)


class AuthTable(tag: Tag) extends Table[AuthEntry](tag, "user_auth") {

  // unique user id for ReaderDeck
	def uid             = column[Id]("user_id", O.NotNull, O.PrimaryKey)

  // Provider used for authentication. Id of user for the
  // provider and provider identifier
  // Example -
  // for Local (ReaderDeck)
  //   userId = emailId
  //   provider = readerdeck
  // for Google -
  //   userId - google provided id
  //   provider = google
  def providerUserId  = column[ProviderUserId]("provider_user_id", O.NotNull)
  def providerId      = column[ProviderId]("provider_id", O.NotNull)

  // whether the auth is oAuth 1, oAuth 2, Local
  def authMethod      = column[String]("auth_method", O.NotNull)

  // oAuth 1 - contains Token, and
  def token           = column[Option[Token]]("token")
  def secret          = column[Option[Secret]]("secret")

  // oAuth 2
  def accessToken     = column[Option[AccessToken]]("access_token")
  def tokenType       = column[Option[TokenType]]("token_type")
  def expiresIn       = column[Option[ExpiresIn]]("expires_in")
  def refreshToken    = column[Option[RefreshToken]]("refresh_token")

  // for Local
  def hash            = column[Option[Hash]]("hash")
  def salt            = column[Option[Salt]]("salt")
  def iteration       = column[Option[Iteration]]("iteration")

  // others
  def isDeleted       = column[Char]("is_deleted", O.NotNull, O.Default('N'))
  def createdDate     = column[DateTime]("created_dt", O.NotNull)
  def updatedDate     = column[DateTime]("updated_dt", O.NotNull)

  def idx = index("idx_a", (updatedDate), unique = false)

  def * : ProvenShape[AuthEntry] = {
    val shaped =
        ( uid,
          providerUserId,
          providerId,
          authMethod,
          token,
          secret,
          accessToken,
          tokenType,
          expiresIn,
          refreshToken,
          hash,
          salt,
          iteration,
          isDeleted,
          createdDate.?,
          updatedDate).shaped

    shaped <> ({
        tuple =>
          val oAuth1Info: Option[OAuth1Info] = (tuple._5, tuple._6)
          val oAuth2Info: Option[OAuth2Info] = (tuple._7, tuple._8, tuple._9, tuple._10)
          val passwordInfo: Option[PasswordInfo] = (tuple._11, tuple._12, tuple._13)

          AuthEntry(
            uid = tuple._1,
            provider = Provider (
              userId = tuple._2,
              providerId = tuple._3,
              authInfo =
                    oAuth1Info orElse
                    oAuth2Info orElse
                    passwordInfo getOrElse NoAuthInfo
            ),
            tuple._14,
            tuple._15,
            tuple._16
          )
      }, {
        (entry: AuthEntry) =>
          Some {
            (
              entry.uid,

              entry.provider.userId,
              entry.provider.providerId,

              entry.provider.authMethod.value,

              entry.provider.oAuth1Info map (_.token),
              entry.provider.oAuth1Info map (_.secret),

              entry.provider.oAuth2Info map (_.accessToken),
              entry.provider.oAuth2Info flatMap (_.tokenType),
              entry.provider.oAuth2Info flatMap (_.expiresIn),
              entry.provider.oAuth2Info flatMap (_.refreshToken),

              entry.provider.passwordInfo map (_.hash),
              entry.provider.passwordInfo flatMap (_.salt),
              entry.provider.passwordInfo flatMap (_.iteration),

              entry.isDeleted,
              entry.createdDate,
              entry.updatedDate
            )
          }
      }
    )
  }

}