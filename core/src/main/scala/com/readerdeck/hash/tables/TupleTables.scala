package com.readerdeck.hash.tables

import scala.slick.driver.MySQLDriver.simple._
import org.joda.time._
import java.sql.Timestamp
import com.readerdeck.hash.utils.Builder._

trait TupleEntry

/**
 * Type safe Builder class for Tuple Table Entries.
 * Currently just added for convenience. :P
 *
 * IMP: currently each function uses explicit casting. See if it can be avoided
 *
 * @author Kumar Ishan (@kumarishan)
 */
class TupleEntryBuilder[E <: TupleEntry,
  HKT, HK, HE, HSE, HV, HF, HC, HVL, HD, HCD, HUD](

    val keyTypeId: Option[KeyTypeId],
    val keyId: Option[Id],
    val entityId: Option[Id],
    val subEntityId: Option[Id],
    val version: Option[Version],
    val format: Option[Format],
    val compression: Option[Compression],
    val value: Option[Binary],
    val deleted: Option[Boolean],
    val createdDate: Option[DateTime],
    val updatedDate: Option[DateTime]
  ){

  def withKeyType(id: KeyTypeId) =
    new TupleEntryBuilder[E, TRUE, HK, HE, HSE, HV, HF, HC, HVL, HD, HCD, HUD](
      Some(id), keyId, entityId, subEntityId, version, format, compression, value, deleted, createdDate, updatedDate)

  def withKey(id: Id) =
    new TupleEntryBuilder[E, HKT, TRUE, HE, HSE, HV, HF, HC, HVL, HD, HCD, HUD](
      keyTypeId, Some(id), entityId, subEntityId, version, format, compression, value, deleted, createdDate, updatedDate)

  def withEntity(id: Id) =
    new TupleEntryBuilder[E, HKT, HK, TRUE, HSE, HV, HF, HC, HVL, HD, HCD, HUD](
      keyTypeId, keyId, Some(id), subEntityId, version, format, compression, value, deleted, createdDate, updatedDate)

  def withSubEntity(id: Id) =
    new TupleEntryBuilder[E, HKT, HK, HE, TRUE, HV, HF, HC, HVL, HD, HCD, HUD](
      keyTypeId, keyId, entityId, Some(id), version, format, compression, value, deleted, createdDate, updatedDate)

  def withVersion(version: Version) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, TRUE, HF, HC, HVL, HD, HCD, HUD](
      keyTypeId, keyId, entityId, subEntityId, Some(version), format, compression, value, deleted, createdDate, updatedDate)

  def withFormat(format: Format) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, TRUE, HC, HVL, HD, HCD, HUD](
      keyTypeId, keyId, entityId, subEntityId, version, Some(format), compression, value, deleted, createdDate, updatedDate)

  def withCompression(compression: Compression) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, HF, TRUE, HVL, HD, HCD, HUD](
      keyTypeId, keyId, entityId, subEntityId, version, format, Some(compression), value, deleted, createdDate, updatedDate)

  def withValue(value: Binary) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, HF, HC, TRUE, HD, HCD, HUD](
      keyTypeId, keyId, entityId, subEntityId, version, format, compression, Some(value), deleted, createdDate, updatedDate)

  def isDeleted(deleted: Boolean) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, HF, HC, HVL, TRUE, HCD, HUD](
      keyTypeId, keyId, entityId, subEntityId, version, format, compression, value, Some(deleted), createdDate, updatedDate)

  def withCreatedDate(date: DateTime) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, HF, HC, HVL, HD, TRUE, HUD](
      keyTypeId, keyId, entityId, subEntityId, version, format, compression, value, deleted, Some(date), updatedDate)

  def withUpdatedDate(date: DateTime) =
    new TupleEntryBuilder[E, HKT, HK, HE, HSE, HV, HF, HC, HVL, HD, HCD, TRUE](
      keyTypeId, keyId, entityId, subEntityId, version, format, compression, value, deleted, createdDate, Some(date))

}

/**
 * A common interface for all tuple tables, mandating certain columns
 *
 * @author Kumar Ishan (@kumarishan)
 */
trait TupleTable { self: Table[_] =>

  type Entry <: TableElementType with TupleEntry

  def keyTypeId: Column[KeyTypeId]

  def version: Column[Version]
  def format: Column[Format]
  def compression: Column[Compression]
  def value: Column[Binary]

  // a standard common columns required by all
  def isDeleted: Column[Char]
  def createdDate: Column[DateTime]
  def updatedDate: Column[DateTime]
}