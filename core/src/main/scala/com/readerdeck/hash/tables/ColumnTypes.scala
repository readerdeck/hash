package com.readerdeck.hash.tables

// Tuple table columns
case class KeyTypeId(id: Short)

case class Id(id: Long)

case class Version(v: Short)

case class Format(f: Char)

case class Compression(c: Char)

case class Binary(value: Array[Byte])