package com.readerdeck.hash.auth

case class ProviderUserId(value: String)
case class ProviderId(value: String)

case class Token(value: String)
case class Secret(value: String)

case class AccessToken(value: String)
case class TokenType(value: String)
case class ExpiresIn(value: Int)
case class RefreshToken(value: String)

case class Hash(value: Array[Byte])
case class Salt(value: Array[Byte])
case class Iteration(iter: Int)