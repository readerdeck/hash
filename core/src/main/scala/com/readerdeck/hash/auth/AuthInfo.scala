package com.readerdeck.hash.auth

sealed trait AuthInfo
case class PasswordInfo(hash: Hash, salt: Option[Salt], iteration: Option[Iteration])
  extends AuthInfo

case class OAuth1Info(token: Token, secret: Secret) extends AuthInfo

case class OAuth2Info(accessToken: AccessToken, tokenType: Option[TokenType], expiresIn: Option[ExpiresIn],
                      refreshToken: Option[RefreshToken]) extends AuthInfo

case object NoAuthInfo extends AuthInfo