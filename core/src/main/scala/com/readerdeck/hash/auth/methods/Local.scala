package com.readerdeck.hash.auth.methods

import java.security.SecureRandom
import javax.crypto.spec.PBEKeySpec
import javax.crypto.SecretKeyFactory
import java.math.BigInteger
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import scala.util.{Try, Success => TSuccess, Failure => TFailure}
import com.readerdeck.hash.auth._
import argonaut._, Argonaut._

class Local(val info: PasswordInfo) extends AuthMethod {
  import Local._

  type Info = PasswordInfo

  val value = "local"

  def authenticate(cred: Json) = {
    (for {
      password <- +cred --\ "password" map (_.focus) flatMap (_.as[String].toOption)
      hash <- Option(info.hash)
      salt <- info.salt
      iteration <- info.iteration
    } yield isEqual(password.toCharArray, hash, salt, iteration)) getOrElse(false)
  }

  private def isEqual(password: Array[Char], hash: Hash, salt: Salt, iter: Iteration) = {
    (for {
      originalHash <- Option(hash.value)
      testHash <- pbkdf2(password, salt.value, iter.iter, hash.value.length)
    } yield java.util.Arrays.equals(originalHash, testHash)) getOrElse(false)
  }


}

object Local {

  private val PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1"
  private val SALT_BYTE_SIZE = 24
  private val HASH_BYTE_SIZE = 24
  private val PBKDF2_ITERATIONS = 1000

  private def pbkdf2(password: Array[Char], salt: Array[Byte], iter: Int, bytes: Int) =
    Try {
      val spec = new PBEKeySpec(password, salt, iter, bytes * 8)
      val skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM)
      skf.generateSecret(spec).getEncoded
    } match {
      case TSuccess(a) => Option(a)
      case TFailure(ex) => {
        println("Some failure occured while generating hash")
        None
      }
    }

  def apply(info: PasswordInfo) =
    new Local(info)

  def info(password: String) = {
    val random = new SecureRandom
    val salt = new Array[Byte](SALT_BYTE_SIZE)
    random.nextBytes(salt)

    pbkdf2(password.toCharArray, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE) map {
      hash =>
        PasswordInfo(Hash(hash), Some(Salt(salt)), Some(Iteration(PBKDF2_ITERATIONS)))
    }
  }
}