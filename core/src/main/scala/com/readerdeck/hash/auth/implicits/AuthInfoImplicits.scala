package com.readerdeck.hash.auth.implicits

import com.readerdeck.hash.auth._

trait AuthInfoImplicits {

  implicit def tuple2OAuth1Info(tuple: (Option[Token], Option[Secret])) = tuple match {
    case (Some(token), Some(secret)) => Some(OAuth1Info(token, secret))
    case _ => None
  }

  implicit def tuple2OAuth2Info(tuple: (Option[AccessToken], Option[TokenType],
                                        Option[ExpiresIn], Option[RefreshToken])) = tuple match {
    case (Some(accessToken), tokenType, expiresIn, refreshToken) =>
      Some(OAuth2Info(accessToken, tokenType, expiresIn, refreshToken))
    case _ => None
  }

  implicit def tuple2PasswordInfo(tuple: (Option[Hash], Option[Salt], Option[Iteration])) =
    tuple match {
      case (Some(hash), salt, iteration) =>
        Some(PasswordInfo(hash, salt, iteration))
      case _ => None
    }
}