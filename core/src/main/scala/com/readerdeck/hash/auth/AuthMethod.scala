package com.readerdeck.hash.auth

// import scalaz._, Scalaz._
import argonaut._, Argonaut._

trait AuthMethod {

  type Info <: AuthInfo

  val value: String

  def info: Info

  def authenticate(cred: Json): Boolean
}

// case class OAuth1(info: OAuth1Info) extends AuthMethod {
//   type Info = OAuth1Info

//   val value = "oauth1"
// }

// case class OAuth2(info: OAuth2Info) extends AuthMethod {
//   type Info = OAuth2Info

//   val value = "oauth2"
// }

case class UnknownMethod(info: AuthInfo) extends AuthMethod {
  type Info = AuthInfo

  val value = "unknown"

  def authenticate(cred: Json) = false
}