package com.readerdeck.hash.auth

import methods._
import argonaut._, Argonaut._
import org.joda.time._
import com.readerdeck.hash.tables._

trait Provider {
  type AM <: AuthMethod

  val provider: String

  val userId: ProviderUserId
  def providerId = ProviderId(provider)

  def authMethod: AM

  def oAuth1Info: Option[OAuth1Info] = None
  def oAuth2Info: Option[OAuth2Info] = None
  def passwordInfo: Option[PasswordInfo] = None

  def authenticate(cred: Json): Boolean =
    authMethod.authenticate(cred: Json)

  def entry(uid: Id): AuthEntry =
    AuthEntry(uid, this, 'N', None, DateTime.now)
}

object Provider {

  // to be used to create new Provider using details
  def apply(cred: Json): Option[Provider] =
    (+cred --\ "provider" map (_.focus) flatMap (_.as[String].toOption)) flatMap {
      case "readerdeck" =>
        for {
          password <- +cred --\ "password" map (_.focus) flatMap (_.as[String].toOption)
          info <- Local.info(password)
          userId <- +cred --\ "email" map (_.focus) flatMap (_.as[String].toOption)
        } yield new LocalProvider(ProviderUserId(userId), info)
      case _ => None
    }

  // to be used to instantiate appropriate provider with details
  def apply(userId: ProviderUserId, providerId: ProviderId,
            authInfo: AuthInfo): Provider = {

    (providerId.value match {
      case "readerdeck" =>
        authInfo match {
            case p: PasswordInfo =>
              Some(new LocalProvider(userId, p))
            case _ => None
        }
      // case "facebook" =>
      // case _ =>
    }) getOrElse (
      new UnknownProvider(
        userId,
        providerId.value,
        authInfo)
    )
  }
}

class LocalProvider(val userId: ProviderUserId, info: PasswordInfo) extends Provider {
  val provider = "readerdeck"
  type AM = Local

  lazy val authMethod = Local(info)
  override def passwordInfo = Some(authMethod.info)
}

// class TwitterProvider(val userId: ProviderUserId) extends Provider {
//   val provider = "twitter"
// }

// class GoogleProvider(val userId: ProviderUserId) extends Provider {
//   val provider = "google"
// }

// class FacebookProvider(val userId: ProviderUserId) extends Provider {
//   val provider = "facebook"
// }

class UnknownProvider(val userId: ProviderUserId,
                      val provider: String,
                      info: AuthInfo) extends Provider {
  type AM = UnknownMethod

  lazy val authMethod = UnknownMethod(info)
}