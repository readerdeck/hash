package com.readerdeck.hash

import tables._
import scala.slick.driver.MySQLDriver.simple._

/**
 * IMP TO DO - an alternative implementation of
 * queryFor function with has type parameter only
 *
 * @author Kumar Ishan (@kumarishan)
 */
trait BaseDAO {
  def queryFor[T <: Table[_] with TupleTable](kt: KeyType[T, _]) =
    kt.tableQuery

  def nativeFor[T <: Table[_] with TupleTable](kt: KeyType[T, _]) =
    kt.table
}