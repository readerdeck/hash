package com.readerdeck.hash

import Implicits._
import tables._
import scala.slick.driver.MySQLDriver.simple._

/**
 * KeyType gives the value of keytype column and the associated
 * MySQL table.
 *
 * IMP TO DO: Later redefining KeyType such that query in @BaseDAO
 * cane be defined as def queryFor[K <: KeyType[_]]: TableQuery[...]
 *
 * @author Kumar Ishan (@kumarishan)
 */
trait KeyType[T <: Table[_] with TupleTable, V] {
  val keyTypeId : Short
  val table : TableQuery[T]
  def tableQuery = table.filter({
    x =>
      x.keyTypeId === KeyTypeId(keyTypeId) &&
      x.isDeleted === 'N'
    })

  def serialize(v: V): Binary
  def deserialize(b: Binary): V

  def builder(value: V): TupleEntryBuilder[T#Entry,_,_,_,_,_,_,_,_,_,_,_]

  def deconstruct(entry: T#Entry): V
}