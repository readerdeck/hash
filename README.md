# Hash

A DAL for using MySQL as Key Value store

Dependency
```
libraryDependencies += "com.readerdeck" % "hash-core" % "1.0.0"
```

## Data model design

### Data tables in MySQL db

There are three types of key value pair tables 

#### Tuple 2 Table - key_tuple_2
```
create table if not exists `key_tuple_2` (
    `key_type`     smallint unsigned not null,
    `key_id`       bigint unsigned not null,
    
    `version`      bigint unsigned not null,
    `format`       char(1) not null default 'S',
    `compression`  char(1) not null default 'F',
    `value`        blob not null,

    `is_deleted`   char(1) not null default 'N',
    `created_dt`   int unsigned not null,
    `updated_dt`   int unsigned not null,
    
    PRIMARY KEY(`key_type`, `key_id`),
    INDEX(`updated_dt`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC CHARACTER SET utf8;

#### Tuple 3 Table - key_tuple_3

```
create table if not exists `key_tuple_3` (
    `key_type`     smallint unsigned not null,
    `key_id`       bigint unsigned not null,
    `entity_id`    bigint unsigned not null,
    
    `version`      bigint unsigned not null,
    `format`       char(1) not null default 'S',
    `compression`  char(1) not null default 'F',
    `value`        blob not null,

    `is_deleted`   char(1) not null default 'N',
    `created_dt`   bigint unsigned not null,
    `updated_dt`   bigint unsigned not null,
    
    PRIMARY KEY(`key_type`, `key_id`, `entity_id`),
    INDEX(`updated_dt`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC CHARACTER SET utf8;
```

#### Tuple 4 Table - key_tuple_4

```
create table if not exists `key_tuple_4` (
    `key_type`      smallint unsigned not null,
    `key_id`        bigint unsigned not null,
    `entity_id`     bigint unsigned not null,
    'sub_entity_id' bigint unsigned not null,
    
    `version`       bigint unsigned not null,
    `format`        char(1) not null default 'S',
    `compression`   char(1) not null default 'F',
    `value`         blob not null,
    
    `is_deleted`    char(1) not null default 'N',
    `created_dt`    bigint unsigned not null,
    `updated_dt`    bigint unsigned not null,

    PRIMARY KEY(`key_type`, `key_id`, `entity_id`),
    INDEX(`updated_dt`)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC CHARACTER SET utf8;
```
